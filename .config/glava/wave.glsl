/* Min (vertical) line thickness */
#define MIN_THICKNESS 1
/* Max (vertical) line thickness */
#define MAX_THICKNESS 6
/* How quickly the gradient transitions, in pixels */
#define GRADIENT 80
/* Base color to use, distance from center will multiply the RGB components */
/*#define BASE_COLOR @fg:vec4(0.7, 0.2, 0.45, 1)*/
#define BASE_COLOR @fg:mix(#3366b2, #AA0000, clamp(GRADIENT, 0, 1))
/* Amplitude */
#define AMPLIFY 500
/* Outline color */
/*#define OUTLINE @bg:vec4(0.15, 0.15, 0.15, 1)*/
#define OUTLINE @bg:mix(#3366b2, #AA0000, clamp(GRADIENT, 0, 1))
