"        _                    
" __   _(_)_ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
" (_)_/ |_|_| |_| |_|_|  \___|
"                             
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valoric/YouCompleteMe'
Plugin 'c.vim'
Plugin 'vim-python/python-syntax'
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'vim-pandoc/vim-pandoc-syntax'
Plugin 'tranvansang/octave.vim'
Plugin 'lervag/vimtex'
Plugin 'dylanaraps/wal.vim'
Plugin 'stevearc/vim-arduino'
"Plugin 'chrisbra/csv.vim'
"Plugin 'vim-scripts/vimtex'
"Plugin 'synthax/retdecdsm' 

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on
 
set number				" Enables line numbering.
set relativenumber			" Enables relative number display.
set showcmd				" Enables vim to show command buffer.
set incsearch				" Enables incremental searching while typing.
colorscheme wal


" SETS INSERT MODE'S KEY BINDINGS
imap ( () <++><Esc>5hi
imap <Right>9 (<Esc>ldt>xa
imap [ [] <++><Esc>5hi
imap <Right>[ [<Esc>ldt>xa
imap { {} <++><Esc>5hi
imap <Left>[ {<Esc>ldt>xa
imap <Right>' ''<Esc>i 
imap <Left>' ""<Esc>i
imap <Right>4 $$<Esc>i
imap <Right>. <++><Esc>li
imap <Left>, <> <++><Esc>5hi
imap <Right>, <><Esc>i
imap <Space><Space> <Esc>/<++><Return>ca>

" SETS BACKSPACE AS <ESC> IN BOTH VISUAL AND INSERT MODES
inoremap <Backspace> <Esc>
vnoremap <Backspace> <Esc>

" SETS KEYBINDINGS FOR HEXMODE
nnoremap <C-H> :Hexmode<CR>
"inoremap <C-H> :Hexmode<CR>
"vnoremap <C-H> :Hexmode<CR>

" SETS NORMAL MODE'S KEY BINDINGS
nmap <Space><Space> <Esc>/<++><Return>ca>
nnoremap <C-E> :Vexplore<Return>
let g:netrw_winsize = 25

" SETS CONFIG. TO ARDUINO PLUGIN
let g:arduino_cmd = '/snap/bin/arduino'
let g:arduino_dir = '/snap/arduino'
let g:arduino_home_dir = $HOME ".arduino15"

" SETS VISUAL MODE'S KEY BINDINGS
vnoremap ( <Esc>`>a)<Esc>`<i(<Esc>
vnoremap { <Esc>`>a}<Esc>`<i{<Esc>
vnoremap [ <Esc>`>a]<Esc>`<i[<Esc>
vnoremap ' <Esc>`>a'<Esc>`<i'<Esc>
vnoremap " <Esc>`>a"<Esc>`<i"<Esc>
vnoremap 4 <Esc>`>a$<Esc>`<i$<Esc>
vnoremap 8 <Esc>`>a*<Esc>`<i*<Esc>
vnoremap * <Esc>`>a**<Esc>`<i**<Esc>
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" SETS BELOW TERMINAL ALIAS
cnoreabbrev bt below terminal
let g:term_winsize = 25

" SETS SPELL CHECK MAIN LANGUAGES AND KEY MAPPING
map <F5> :setlocal spell! spelllang=en_us<CR>
map <F4> :setlocal spell! spelllang=pt<CR>
call pathogen#infect()

" POWERLINE MANAGEMEN
set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

" Always show statusline
set laststatus=2

" Use 256 colours (Use this setting only if your terminal supports 256
" colours)
set t_Co=256
" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
nnoremap z/ :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>
function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    echo 'Highlight current word: off'
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    echo 'Highlight current word: ON'
    return 1
  endif
endfunction
