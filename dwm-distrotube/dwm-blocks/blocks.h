//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/	 	/*Update Interval*/	/*Update Signal*/
	{" ", "~/scripts/dwm-blocks/upt",		        60,		            2},

	{" RAM: ", "~/scripts/dwm-blocks/memory",	        	6,		            1},

	//{" Vol: ", "~/scripts/dwm-blocks/volume",			2,		            10},

	{" Mem: ", "~/scripts/dwm-blocks/disk",	        		6,		            1},

	{" Load: ", "~/scripts/dwm-blocks/load",        		3,		            1},

	{" ", "~/scripts/dwm-blocks/clock",				5,		            0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
