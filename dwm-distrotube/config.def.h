/* See LICENSE file for copyright and license details. */
/* appearance */
static const unsigned int borderpx    = 2;        /* border pixel of windows */
static const unsigned int gappx       = 3;        /* gap pixel between windows */
static const unsigned int snap        = 32;       /* snap pixel */
static const int showbar              = 1;        /* 0 means no bar */
static const int topbar               = 1;        /* 0 means bottom bar */
static const int horizpadbar          = 6;        /* horizontal padding for statusbar */
static const int vertpadbar           = 7;        /* vertical padding for statusbar */
static const char *fonts[]            = {"Terminus:size=9:antialias=true:autohint=true",
                                         "Hack:size=8:antialias=true:autohint=true",
                                         "JoyPixels:size=10:antialias=true:autohint=true"};
static const char col_gray1[]         = "#282a36";
static const char col_gray2[]         = "#000000"; /* border color unfocused windows */
static const char col_gray3[]         = "#96b5b4";
static const char col_gray4[]         = "#d7d7d7";
static const char urgbordercolor[]    = "#ff0000";
static const char col_cyan[]          = "#505050"; /*"#924441";*/ /* border color focused windows and tags */
/* bar opacity 
 * 0xff is no transparency.
 * 0xee adds wee bit of transparency.
 * Play with the value to get desired transparency.
 */
static const unsigned int baralpha    = 0xff; /*0xee;  */ 
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]        = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, 0x00 /*col_gray1*/, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
// PYWAL INTEGRATION
// #include "/home/joao/.cache/wal/colors-wal-dwm.h"
static const char *alphas[][3]        = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
//static const unsigned int alphas[][3] = {
//	/*               fg      bg        border     */
//	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
//	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
//};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Firefox",  NULL,       NULL,       NULL,         0,           -1 },
	{ "snes9x",   NULL,       NULL,       NULL,         1,           -1 },
	{ "arduino",  NULL,       NULL,       NULL,         1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "HHH",      grid },
  	{ "[@]",      spiral },
  	{ "[\\]",     dwindle },
 	{ "|M|",      centeredmaster },
 	{ ">M>",      centeredfloatingmaster },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]    = { "dmenu_run", "-p", "Run: ", NULL };
static const char *termcmd[]     = { "st", NULL };
static const char *tabtermcmd[]  = { "tabbed", "-r 2", "st", "-w", "''", NULL };

#include "selfrestart.c"

static Key keys[] = {
	/* modifier             	key        function        argument */
	{ MODKEY,     			XK_m, 	   spawn,          {.v = dmenucmd } },
	{ MODKEY,               	XK_Return, spawn,          {.v = termcmd } },
	{ Mod1Mask,             	XK_Return, spawn,          {.v = tabtermcmd } },
	{ MODKEY,               	XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,     	XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,     	XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,               	XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,               	XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,           	XK_equal,  incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,            	XK_minus,  incnmaster,     {.i = -1 } },
	{ MODKEY,               	XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,               	XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -3 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +3 } },
	{ MODKEY|ControlMask,           XK_equal,  setgaps,        {.i = 0  } },
	{ MODKEY|ControlMask,   	XK_Return, zoom,           {0} },
	{ MODKEY,               	XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,     	XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,     	XK_c,      self_restart,   {0} },

    /* Layout manipulation */
	{ MODKEY|ShiftMask,     XK_space,  cyclelayout,    {.i = -1 } },
	{ MODKEY,     		XK_space,  cyclelayout,    {.i = +1 } },
	{ MODKEY,               XK_Tab,    setlayout,      {0} },
	{ MODKEY|ShiftMask, 	XK_f,  	   fullscreen, 	   {0} },
	{ MODKEY,     		XK_f,  	   togglefloating, {0} },

    /* Switch to specific layouts */
	{ MODKEY|ControlMask,               XK_1,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ControlMask,               XK_2,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ControlMask,               XK_3,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ControlMask,               XK_4,      setlayout,      {.v = &layouts[3]} },
 	{ MODKEY|ControlMask,               XK_5,      setlayout,      {.v = &layouts[3]} },
 	{ MODKEY|ControlMask,               XK_6,      setlayout,      {.v = &layouts[4]} },

	{ MODKEY,               XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,     XK_0,      tag,            {.ui = ~0 } },

    /* Switching between monitors */
	{ MODKEY,               XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,               XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,     XK_period, tagmon,         {.i = +1 } },
 	{ MODKEY,               XK_minus,  setgaps,        {.i = -5 } },
 	{ MODKEY,               XK_equal,  setgaps,        {.i = +5 } },
 	{ MODKEY|ShiftMask,     XK_equal,  setgaps,        {.i = 0  } },
	
    /* Apps Launched with SUPER + ALT + KEY */
	{ MODKEY|ShiftMask,       XK_i,    spawn,          CMD("~/Apps/firefox/firefox") },
	{ MODKEY,       	  XK_c,    spawn,          CMD("st -e cmatrix") },
	{ MODKEY|ShiftMask,       XK_x,    spawn,          CMD("shutdown now") },
	{ MODKEY,        	  XK_r,    spawn,          CMD("st -e ranger") },
	{ MODKEY,        	  XK_g,    spawn,          CMD("compton && glava --desktop") },
	{ MODKEY,        	  XK_o,    spawn,          CMD("st -e octave --no-gui") },
	{ MODKEY|ShiftMask,       XK_h,    spawn,          CMD("st -e htop") },
	{ MODKEY|ShiftMask,       XK_v,    spawn,          CMD("st -e vim .vimrc") },
	{ MODKEY|ShiftMask,       XK_d,    spawn,          CMD("st -e vim ~/dwm-distrotube/config.def.h") },
	{ MODKEY,        	  XK_v,    spawn,          CMD("/snap/bin/vlc") },
	{ MODKEY,        	  XK_p,    spawn,          CMD("~/scripts/pdf_menu") },
	{ MODKEY|ShiftMask,       XK_m,    spawn,          CMD("~/scripts/mus_script") },
	{ MODKEY|ShiftMask,       XK_n,    spawn,          CMD("~/scripts/net_script") },
	{ MODKEY,       	  XK_i,    spawn,          CMD("~/scripts/wall_script") },
	{ MODKEY|ShiftMask,       XK_b,    spawn,          CMD("~/Desktop/Códigos/RANDOM/SDL/SDL_block") },
	{ 0, 0x1008FF12, 	   	   spawn,          SHCMD("amixer -D pulse sset Master toggle") },
	{ 0, 0x1008FF13,         	   spawn,          SHCMD("amixer -D pulse sset Master 10\%+ unmute") },
	{ 0, 0x1008FF11,         	   spawn,          SHCMD("amixer -D pulse sset Master 10\%- unmute") },
	{ 0, XK_Print,         	    	   spawn,          SHCMD("gnome-screenshot") },
	
	TAGKEYS(                  XK_1,          0)
	TAGKEYS(                  XK_2,          1)
	TAGKEYS(                  XK_3,          2)
	TAGKEYS(                  XK_4,          3)
	TAGKEYS(                  XK_5,          4)
	TAGKEYS(                  XK_6,          5)
	TAGKEYS(                  XK_7,          6)
	TAGKEYS(                  XK_8,          7)
	TAGKEYS(                  XK_9,          8)
	{ MODKEY|ShiftMask,       XK_e,	   quit,		   	{0} },
    	{ MODKEY|ShiftMask,       XK_r,    quit,           		{1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click           event mask   button          function        argument */
	{ ClkLtSymbol,     0,           Button1,        setlayout,      {0} },
	{ ClkLtSymbol,     0,           Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,     0,           Button2,        zoom,           {0} },
	{ ClkStatusText,   0,           Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,    MODKEY,      Button1,        movemouse,      {0} },
	{ ClkClientWin,    MODKEY,      Button2,        togglefloating, {0} },
	{ ClkClientWin,    MODKEY,      Button3,        resizemouse,    {0} },
	{ ClkTagBar,       0,           Button1,        view,           {0} },
	{ ClkTagBar,       0,           Button3,        toggleview,     {0} },
	{ ClkTagBar,       MODKEY,      Button1,        tag,            {0} },
	{ ClkTagBar,       MODKEY,      Button3,        toggletag,      {0} },
};
